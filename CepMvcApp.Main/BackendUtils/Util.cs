﻿using System.Diagnostics;

namespace CepMvcApp.Main.BackEndUtils
{
    public class Util
    {
        public static bool IsDebugCompilation(bool checkIsDebuggerAttached = true)
        {
            #if (DEBUG)
            {
                return checkIsDebuggerAttached ? Debugger.IsAttached : true;
            }
            #endif

            #pragma warning disable CS0162
            return false;
            #pragma warning restore CS0162
        }
    }
}