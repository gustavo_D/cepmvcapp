﻿using System.Web;
using System.Web.Optimization;

namespace CepMvcApp.Main
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // helpers bundle
            bundles.Add(new ScriptBundle("~/bundles/helpers")
                .Include("~/Scripts/helpers/action-helpers.js")
                .Include("~/Scripts/helpers/dataTables-helpers.js")
                .Include("~/Scripts/helpers/jqGrid-helpers.js")
                .Include("~/Scripts/helpers/text-input-helpers.js")
                .Include("~/Scripts/helpers/url-helpers.js")
                );


            // bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.min.js"));
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include("~/Content/bootstrap.min.css"));

            // Vendor scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-2.1.4.min.js"));

            //Jquery unobtrusive
            bundles.Add(new ScriptBundle("~/bundles/jquery-unobtrusive")
                .Include("~/Scripts/jquery.unobtrusive-ajax.min.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.min.js")
                );


            // jQuery UI
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include("~/Scripts/plugins/jquery-ui/jquery-ui.min.js"));

            bundles.Add(new StyleBundle("~/Content/jqueryui-css").Include(
                        "~/Content/plugins/jquery-ui/jquery-ui.min.css",
                        "~/Content/plugins/jquery-ui/jquery-ui.structure.min.css",
                        "~/Content/plugins/jquery-ui/jquery-ui.theme.min.css"));

            //jQGrid
            bundles.Add(new ScriptBundle("~/plugins/jqGrid-js").IncludeDirectory("~/Scripts/plugins/jqGrid", "*.js", true));
            bundles.Add(new StyleBundle("~/Content/jqGrid-css").IncludeDirectory("~/Content/plugins/jqGrid", "*.css", true));

            //Toastr - Notifications
            bundles.Add(new ScriptBundle("~/plugins/toastr-js").Include("~/Scripts/plugins/toastr/toastr.min.js", "~/Scripts/helpers/notification-helpers.js"));
            bundles.Add(new StyleBundle("~/Content/toastr-css").Include("~/Content/plugins/toastr/toastr.min.css"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/plugins/jquery.validate/jquery.validate.min.js"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include("~/Scripts/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include("~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            //metsiMenu 
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include("~/Scripts/plugins/metisMenu/metisMenu.min.js"));

            //pace
            bundles.Add(new ScriptBundle("~/plugins/pace").Include("~/Scripts/plugins/pace/pace.min.js"));

            //Select2
            bundles.Add(new StyleBundle("~/Content/select2").Include("~/Content/plugins/select2/select2.min.css"));
            bundles.Add(new ScriptBundle("~/plugins/select2").Include("~/Scripts/plugins/select2/select2.full.min.js"));

            //Select2
            bundles.Add(new StyleBundle("~/Content/chosen").Include("~/Content/plugins/chosen/chosen.css"));
            bundles.Add(new ScriptBundle("~/plugins/chosen").Include("~/Scripts/plugins/chosen/chosen.jquery.js"));

            // Font Awesome icons
            bundles.Add(new StyleBundle("~/font-awesome/css").Include("~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            // InputMask
            bundles.Add(new ScriptBundle("~/bundles/inputmask").Include(
                        "~/Scripts/plugins/jquery.inputmask/inputmask.js",
                        "~/Scripts/plugins/jquery.inputmask/jquery.inputmask.js",
                        "~/Scripts/plugins/jquery.inputmask/inputmask.extensions.js",
                        "~/Scripts/plugins/jquery.inputmask/inputmask.date.extensions.js",
                        "~/Scripts/plugins/jquery.inputmask/inputmask.numeric.extensions.js"));


            // dataTables css styles
            bundles.Add(new StyleBundle("~/Content/dataTables-css").Include(
                      "~/Content/plugins/dataTables/dataTables.bootstrap.css",
                      "~/Content/plugins/dataTables/dataTables.responsive.css",
                      "~/Content/plugins/dataTables/dataTables.tableTools.min.css"));

            // dataTables 
            bundles.Add(new ScriptBundle("~/plugins/dataTables").Include(
                      "~/Scripts/plugins/dataTables/jquery.dataTables.js",
                      "~/Scripts/plugins/dataTables/dataTables.bootstrap.js",
                      "~/Scripts/plugins/dataTables/dataTables.responsive.js",
                      "~/Scripts/plugins/dataTables/dataTables.tableTools.min.js",
                      "~/Scripts/plugins/dataTables/dataTables.select.min.js",
                      "~/Scripts/plugins/dataTables/jquery.dataTables.rowGrouping.js"
                      ));

            //momentjs
            bundles.Add(new ScriptBundle("~/plugins/momentjs").Include("~/Scripts/plugins/momentJs/moment.min.js"));

            //datepicker - JS
            bundles.Add(new ScriptBundle("~/plugins/datepicker").Include(
                "~/Scripts/plugins/bootstrap-datepicker/bootstrap-datepicker.js",
                "~/Scripts/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js"
                ));

            //datepicker - CSS
            bundles.Add(new StyleBundle("~/Content/datepicker-css").Include("~/Content/plugins/bootstrap-datepicker/datepicker3.css"));

            //numeric
            bundles.Add(new ScriptBundle("~/plugins/numeric").Include("~/Scripts/plugins/numeric/jquery.numeric.min.js"));

            //sweetalert
            bundles.Add(new ScriptBundle("~/plugins/sweetalert").Include("~/Scripts/plugins/sweetalert/sweetalert.min.js"));
            bundles.Add(new StyleBundle("~/Content/sweetalert").Include("~/Content/plugins/sweetalert/sweetalert.css"));
        }
    }
}
