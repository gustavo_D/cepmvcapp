﻿namespace CepMvcApp.Main.Helpers.Factories
{
    public class JsonResponseFactory
    {
        public JsonResponseFactory() {;}

        public static object ErrorResponse(string error)
        {
            return new { Success = false, Message = error ?? "Erro inesperado" };
        }

    }
}