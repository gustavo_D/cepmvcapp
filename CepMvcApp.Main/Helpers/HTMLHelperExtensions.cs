﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace CepMvcApp.Main
{
    /// <summary>
    /// Estas extensões da classe <seealso cref="HtmlHelper"/> são usadas pelo Razor
    /// </summary>
    public static class HTMLHelperExtensions
    {

        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }

        public static string IsSelected(this HtmlHelper html, string[] controllers = null)
        {
            string cssClass = "active";
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            bool containsController = false;

            containsController = (controllers != null && controllers.Contains(currentController)) ? true : false;

            return containsController ? cssClass : String.Empty;
        }

        public static string AbsoluteUrlNoController(this UrlHelper url, string actionName, object routeValues = null)
        {
            string scheme = url.RequestContext.HttpContext.Request.Url.Scheme;
            string controller = url.RequestContext.RouteData.GetRequiredString("controller");
            return url.Action(actionName, controller, routeValues, scheme);
        }
    }
}