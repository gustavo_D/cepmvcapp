﻿using System.ComponentModel.DataAnnotations;

namespace CepMvcApp.Main.Models
{
    public class LocalidadesViewModel
    {
        public LocalidadesViewModel() {; }

        public long ID { get; set; }

        [Required(ErrorMessage = "Por favor preencha o Código IBGE")]
        [RegularExpression("([0-9]{7})", ErrorMessage = "Código IBGE inválido")]
        public string CodigoIBGE { get; set; }


        [StringLength(10, MinimumLength = 1, ErrorMessage = "Gia inválido")]
        [RegularExpression("([0-9]{1,10})", ErrorMessage = "Gia inválido")]
        public string Gia { get; set; }

        [RegularExpression("^[0-9]{1,10}$", ErrorMessage = "Siafi inválido")]
        public string Siafi { get; set; }

        public string NomeLocalidade { get; set; }
        public long IDUnidadeFederativa { get; set; }
        public long IDDiscagemDiretaDistancia { get; set; }


    }

}