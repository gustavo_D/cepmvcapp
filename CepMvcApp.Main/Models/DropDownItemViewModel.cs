﻿namespace CepMvcApp.Main.Models
{
    public class DropDownItemViewModel
    {
        public DropDownItemViewModel() {;}

        public long id { get; set; }

        public string text { get; set; }
    }
}