﻿using System.ComponentModel.DataAnnotations;

namespace CepMvcApp.Main.Models
{
    public class CepViewModel
    {
        public CepViewModel() {;}

        public long ID { get; set; }

        [Display(Name = "CEP")]
        [Required(ErrorMessage = "Por favor preencha o CEP")]
        [StringLength(9, MinimumLength = 8, ErrorMessage = "CEP inválido")]
        [RegularExpression("([0-9-]*$)", ErrorMessage = "CEP inválido")]
        public string Valor { get; set; }

        [Display(Name = "Código IBGE")]
        [Required(ErrorMessage = "Por favor preencha o Código IBGE")]
        [RegularExpression("([0-9]{7})", ErrorMessage = "Código IBGE inválido")]
        public string CodigoIBGE { get; set; }

        public string NomeUnidadeFederativa { get; set; }
        public string NomeLocalidade { get; set; }
        public string NomeBairro { get; set; }


        [Display(Name = "Logradouro")]
        [StringLength(150, MinimumLength = 0, ErrorMessage = "Logradouro inválido")]
        public string Logradouro { get; set; }

        [Display(Name = "Complemento")]
        [StringLength(150, MinimumLength = 0, ErrorMessage = "Complemento inválido")]
        public string Complemento { get; set; }
        

        [Display(Name = "Gia")]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "Gia inválido")]
        [RegularExpression("([0-9]{1,10})", ErrorMessage = "Gia inválido")]
        public string Gia { get; set; }

        [Display(Name = "Siafi")]
        [RegularExpression("^[0-9]{1,10}$", ErrorMessage = "Siafi inválido")]
        public string Siafi { get; set; }

        [Display(Name = "Data Cadastro")]
        [DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.DateTime, ErrorMessage = "Formato de Data Invalido")]
        public string DataCadastro { get; set; }

        [Required]
        [Display(Name = "Unidade Federativa")]
        [Range(1, long.MaxValue, ErrorMessage = "Por favor selecione a Unidade Federativa")]
        public long IDUnidadeFederativa { get; set; }

        [Required]
        [Display(Name = "Localidade")]
        [Range(1, long.MaxValue, ErrorMessage = "Por favor selecione a Localidade")]
        public long IDLocalidade { get; set; }

        [Display(Name = "Bairro")]
        [Range(1, long.MaxValue, ErrorMessage = "Por favor selecione o Bairro")]
        public long? IDBairro { get; set; }


        [Required]
        [Display(Name = "DDD")]
        [Range(1, long.MaxValue, ErrorMessage = "Por favor selecione o DDD")]
        public long IDDiscagemDiretaDistancia { get; set; }
    }
}