﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Data.Entity;
using CepMvcApp.Data;
using CepMvcApp.Main.Models;
using CepMvcApp.Model;
using System.Collections.Generic;

namespace CepMvcApp.Main.Controllers
{
    public class LocalidadeController : BaseController
    {
        public JsonResult ListDropDown (long? CodigoIBGE)
        {
            using (var db = new EntitiesDbContext())
            {
                List<LocalidadesIBGE> lista;
                var codigoIBGEConsulta = CodigoIBGE.HasValue ? CodigoIBGE.ToString() : null;
                lista = CodigoIBGE.HasValue ? db.LocalidadesIBGE.Where(item => item.CodigoIBGE == codigoIBGEConsulta).AsNoTracking().ToList()
                                            : db.LocalidadesIBGE.AsNoTracking().ToList();

                var ret = lista.Select(item => new DropDownItemViewModel { id = long.Parse(item.CodigoIBGE), text = item.Nome }).OrderBy(p => p.text).ToList();
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SalvarViaAjax(LocalidadesViewModel dados)
        {
            using (var db = new EntitiesDbContext())
            {
                bool existe = db.LocalidadesIBGE.Any(p => p.CodigoIBGE == dados.CodigoIBGE);
                if (existe)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                var novaLocalidade = new LocalidadesIBGE()
                {
                    CodigoIBGE = dados.CodigoIBGE,
                    Gia = dados.Gia,
                    Siafi = dados.Siafi,
                    Nome = dados.NomeLocalidade,
                    IDUnidadeFederativa = dados.IDUnidadeFederativa,
                    IDDiscagemDiretaDistancia = dados.IDDiscagemDiretaDistancia,
                };

                db.LocalidadesIBGE.Add(novaLocalidade);
                db.SaveChanges();
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}