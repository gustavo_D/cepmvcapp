﻿using System.Text;
using System.Web.Mvc;
using System.Linq;
using System.Data.Entity;
using System.Net;
using CepMvcApp.Data;
using CepMvcApp.Main.Models;
using CepMvcApp.Model;

namespace CepMvcApp.Main.Controllers
{
    public class CepController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult List()
        {
            /* TODO: não é papel do Controller acessar *
             * diretamente a camada de persistência!   */
            using (var db = new EntitiesDbContext())
            {
                var data =   db.CodigosEnderecamentoPostal
                            .ToList()
                            .Select(d => new CepViewModel()
                            {
                                ID = d.ID,
                                Valor = d.Valor,
                                CodigoIBGE = d._LocalidadesIBGE.CodigoIBGE,
                                NomeUnidadeFederativa = d._UnidadesFederativas.Nome,
                                NomeLocalidade = d._LocalidadesIBGE.Nome,
                                NomeBairro = d._Bairros != null ? d._Bairros.Nome : "",
                                Logradouro = d.Logradouro,
                                Complemento = d.Complemento
                            })
                            .ToList();

                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetDadosBuscadorCep(long cep)
        {
            using (var webClient = new WebClient() { Encoding = Encoding.UTF8 })
            {
                var urlConsulta = $"https://viacep.com.br/ws/{cep}/json/";
                var json = webClient.DownloadString(urlConsulta);

                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Form(long id)
        {
            CepViewModel cepViewModel = null;
            if (id == 0)
            {
                cepViewModel = new CepViewModel();
            }
            else
            {
                using (var db = new EntitiesDbContext())
                {
                    var dados = db.CodigosEnderecamentoPostal.Find(id);

                    cepViewModel = new CepViewModel()
                    {
                        Valor = dados.Valor,
                        CodigoIBGE = dados._LocalidadesIBGE.CodigoIBGE,
                        Logradouro = dados.Logradouro,
                        Complemento = dados.Complemento,
                        Gia = dados._LocalidadesIBGE.Gia,
                        Siafi = dados._LocalidadesIBGE.Siafi,
                        IDBairro = dados.IDBairro,
                        IDLocalidade = dados.IDLocalidade,
                        IDUnidadeFederativa = dados.IDUnidadeFederativa,
                        IDDiscagemDiretaDistancia = dados.IDDiscagemDiretaDistancia,
                    };
                }
            }

            return View(cepViewModel);
        }

        [HttpPost]
        public ActionResult SalvarForm(CepViewModel dados)
        {
            using (var db = new EntitiesDbContext())
            {
                var cep = db.CodigosEnderecamentoPostal.Find(dados.ID);

                bool estahEditando = cep != null;
                if (estahEditando)
                {
                    cep.Logradouro = dados.Logradouro;
                    cep.Complemento = dados.Complemento;
                    cep.IDBairro = dados.IDBairro;
                    cep.IDLocalidade = dados.IDLocalidade;
                    cep.IDUnidadeFederativa = dados.IDUnidadeFederativa;
                    cep.IDDiscagemDiretaDistancia = dados.IDDiscagemDiretaDistancia;
                    db.Entry(cep).State = EntityState.Modified;
                }
                else
                {
                    var novoCep = new CodigosEnderecamentoPostal()
                    {
                        Valor = dados.Valor,
                        Logradouro = dados.Logradouro,
                        Complemento = dados.Complemento,
                        IDBairro = dados.IDBairro,
                        IDLocalidade = db.LocalidadesIBGE.Single(p => p.CodigoIBGE == dados.CodigoIBGE).ID,
                        IDUnidadeFederativa = dados.IDUnidadeFederativa,
                        IDDiscagemDiretaDistancia = dados.IDDiscagemDiretaDistancia,
                    };
                    db.CodigosEnderecamentoPostal.Add(novoCep);
                }

                db.SaveChanges();
                ReturnMessage("Cep cadastrado com sucesso");
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(long id)
        {
            using (var db = new EntitiesDbContext())
            {
                var cep = db.CodigosEnderecamentoPostal.Find(id);
                db.CodigosEnderecamentoPostal.Remove(cep);

                db.SaveChanges();
                ReturnMessage("Cep excluído com sucesso");
                return RedirectToAction("Index");
            }
        }
    }
}