﻿using System.Web.Mvc;
using CepMvcApp.Main.BackEndUtils;
using CepMvcApp.Main.Attributes;
using CepMvcApp.Main.Helpers.Factories;

namespace CepMvcApp.Main.Controllers
{
    [ViewModelValidator]
    [HandleError]
    public class BaseController : Controller
    {
        public BaseController() {;}

        protected void ReturnMessage(string message)
        {
            if (message != null && message.Length > 0) TempData["ReturnMessage"] = message;
        }

        protected void ReturnErrorMessage(string message)
        {
            if (message != null && message.Length > 0) TempData["ReturnErrorMessage"] = message;
        }

        protected override void OnException(ExceptionContext exceptionContext)
        {
            var estahDebugando = Util.IsDebugCompilation();
            if (exceptionContext.HttpContext.Request.IsAjaxRequest() && exceptionContext.Exception != null)
            {
                var message = estahDebugando ? exceptionContext.Exception.ToString() : "Ocorreu um erro durante o processamento desta requisição";

                if (exceptionContext.Exception.InnerException != null)
                {
                    var sqlException = exceptionContext.Exception.InnerException.InnerException as System.Data.SqlClient.SqlException;;
                }

                var statusCode = exceptionContext.HttpContext.Response.StatusCode;
                exceptionContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = JsonResponseFactory.ErrorResponse(message)
                };

                ReturnErrorMessage(message);
                exceptionContext.ExceptionHandled = true;
            }
            else
            {
                if (!estahDebugando)
                {
                    RedirectToAction("Error");
                    exceptionContext.ExceptionHandled = true;
                }

                base.OnException(exceptionContext);
            }
        }
    }
}