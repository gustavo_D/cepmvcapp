﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace CepMvcApp.Main.Attributes
{
    public class ViewModelValidatorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool validouModelStates = true;
            try
            {
                var controllerType = filterContext.Controller.GetType();
                var modelStatesPropertyInfo = controllerType.GetProperty("ModelState", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                var modelStates = modelStatesPropertyInfo.GetValue(filterContext.Controller) as ModelStateDictionary;

                validouModelStates = modelStates.IsValid;
            }
            catch (Exception ex)
            {

            }

            if (!validouModelStates)
            {
                throw new HttpRequestValidationException("Algum dos valores recebidos é inválido");
            }
        }
    }
}