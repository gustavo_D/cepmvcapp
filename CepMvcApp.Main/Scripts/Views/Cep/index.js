﻿$(document).ready(function () {
    var dataUrl = getDefaultGridObject().attr("data-url");
    getDefaultGridObject().dataTable({
        drawCallback: function (settings) {
            $('[data-toggle="tooltip"]').tooltip();
            getDefaultGridObject().DataTable().columns.adjust();
        },
        scrollCollapse: true,
        scrollX: true,
        autoWidth: true,
        language: dataTablesLanguageObject(),
        select: dataTablesSelectConfig(),
        ajax: {
            url: dataUrl,
            dataSrc: ''
        },
        order: [],
        columns: [{
            sortable: false, data: null, width: "5%", render: function (o) {
                return defaultActionButtons(o);
            }
        },//0
            {
                title: "CEP", data: "Valor", width: "10%", render: (data) => {
                    return data && data.length && data.length === 8 ? `${data.slice(0, 5)}-${data.slice(5)}` : "CEP inválido";
                }
            },//1
            { title: "Código IBGE", data: "CodigoIBGE", width: "10%" },//2
            { title: "Unidade Federativa", data: "NomeUnidadeFederativa", width: "15%" },//3
            { title: "Localidade", data: "NomeLocalidade", width: "15%" },//4
            { title: "Bairro", data: "NomeBairro", width: "15%" },//5
            { title: "Logradouro", data: "Logradouro", width: "15%" },//6
            { title: "Complemento", data: "Complemento", width: "15%" },//7
        ]
    });
    $("#datatable").css("white-space", "nowrap");
});