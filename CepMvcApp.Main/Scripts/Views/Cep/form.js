﻿$(document).ready(() => {
    $("#buscacep,#cep").inputmask({
        removeMaskOnSubmit: true,
        mask: "99999-999",
        placeholder: "_____-___"
    });

    !parseInt($("#id").val().trim()) ? $("#cep,#gia,#siafi").removeAttr("readonly") : {};

    var possuiCep = $("#cep").val();
    if (possuiCep) {
        setTimeout(() => {
            localidadeDropDownLists($('#localidade'));
            var idLocalidade = $("#valorIdLocalidade").val();
            $("#localidade").val(idLocalidade)
        }, 499);
    }
    inicializarBotaoBuscaCep();
});

function inicializarBotaoBuscaCep() {
    $("#btn-buscacep").on("click", () => {
        var cep = $("#buscacep").val().replace("-", "");
        var urlChamada = $("#btn-buscacep").data("url");
        $.get(urlChamada, { cep: cep })
            .then((data) => {
                if (data) {
                    var obj = JSON.parse(data);

                    $("#cep").val(obj.cep);
                    $("#codigoibge").val(obj.ibge);

                    //XGH
                    localidadeDropDownLists($('#localidade'));
                    var contemLocalidadeRetornada = Window.LocalidadesSelect2Dp ? Window.LocalidadesSelect2Dp.find(p => String(p.id) === obj.ibge) : null;

                    debugger;
                    var idUnidadeFederativa = 1; //BUSCAR TEXTO UF E VER SE ESTÁ CADASTRADO NO BANCO DE DADOS, caso não esteja, cadastrar e retornar a ID e NOME!
                    var idBairro = 1; //BUSCAR TEXTO BAIRRO E VER SE ESTÁ CADASTRADO NO BANCO DE DADOS, caso não esteja, cadastrar e retornar a ID e NOME!
                    var idDiscagemDiretaDistancia = 1; //BUSCAR TEXTO DDD E VER SE ESTÁ CADASTRADO NO BANCO DE DADOS, caso não esteja, cadastrar e retornar a ID e NOME!
                    var idLocalidade = obj.ibge;
                    if (contemLocalidadeRetornada) {

                    } else {
                        var urlSalvarLocalidadeViaAjax = $("#dadosSalvarLocalidadeViaAjax").data("ajax-url");

                        $.post(urlSalvarLocalidadeViaAjax, {
                            dados: {
                                CodigoIBGE: obj.ibge,
                                Gia: obj.gia,
                                Siafi: obj.siafi,
                                NomeLocalidade: obj.localidade,
                                IDUnidadeFederativa: idUnidadeFederativa,
                                IDDiscagemDiretaDistancia: idDiscagemDiretaDistancia
                            }
                        }).then(function (data) {
                            try {
                                $('#localidade').select2('destroy');
                            } catch (ex) {;}
                            $("#valorIdLocalidade").val(obj.ibge);
                            setTimeout(() => { localidadeDropDownLists($('#localidade')); }, 499);
                           });


                    }

                    $("#valorIdUnidadeFederativa").val(idUnidadeFederativa);
                    $("#valorIdLocalidade").val(idLocalidade);
                    $("#valorIdBairro").val("");
                    $("#valorDiscagemDiretaDistancia").val(idDiscagemDiretaDistancia);

                    $("#logradouro").val(obj.logradouro);
                    $("#complemento").val(obj.complemento);
                    $("#gia").val(obj.gia);
                    $("#siafi").val(obj.siafi);
                }
            });
    });
}

function salvarCadastro() {
    $("#form-cadastro").submit();
}