﻿function getDefaultGridObject() { return $("#datatable"); }

function reloadGrid() {getDefaultGridObject().dataTable().api().ajax.reload();}

function dataTablesLanguageObject() {
    return {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma Ascendente",
            "sSortDescending": ": Ordenar colunas de forma Descendente"
        }
    };
}

function dataTablesSelectConfig() {
    return {style: 'os',className: 'active',info: false};
}

function dataTableRedirect(url, id) {
    url = concatIdToUrl(url, id);
    jsRedirectToAction(url);
}

function dataTableDelete(url, id, successCallback) {
    confirmDelete(function () { dataTableRedirect(url, id); });
}

function defaultActionButtons(o) {
    var dataUrlEditar = getDefaultGridObject().attr("data-url-edit");
    var dataUrlDelete = getDefaultGridObject().attr("data-url-delete");

    var stringFuncaoEditar = "dataTableRedirect(\'" + dataUrlEditar + "\'," + o.ID + ")";
    var stringFuncaoDelete = "dataTableDelete(\'" + dataUrlDelete + "\'," + o.ID + ")";

    var htmlString = '<button class="btn btn-white btn-sm " data-container="body" data-toggle="tooltip" data-placement="top" title="Editar" onclick="' + stringFuncaoEditar + '"><i class="fa fa-pencil"></i></button>';
    htmlString += '<button class="btn btn-white btn-sm"  data-container="body" data-toggle="tooltip" data-placement="top" title="Excluir" onclick="' + stringFuncaoDelete + '"><i class="fa fa-trash-o"></i></button>';
    return htmlString;
}