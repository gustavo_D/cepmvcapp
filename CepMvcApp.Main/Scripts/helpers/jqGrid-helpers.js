﻿function getDefaultGridObject() {
    return $("#datatable");
}

function reloadGrid() {
    getDefaultGridObject().trigger('reloadGrid');
}

$(document).on("ready", () => {
    corrigirBackground();
});

$("window").on("resize", () => {
    corrigirBackground();
})

function corrigirBackground() {
    var good = $('#page-wrapper').height();
    $('.index-custom').height(good);
}