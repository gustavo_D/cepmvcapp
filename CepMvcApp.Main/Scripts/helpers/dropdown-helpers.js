﻿function simpleDropDownList(element, placeholderText, ajaxUrl, disabled) {

    return element.select2({
        language: 'pt-BR',
        disabled: disabled,
        width: '100%',
        placeholder: placeholderText,
        minimumResultsForSearch: Infinity,
        allowClear: false,
        ajax: {
            url: function () {
                if (ajaxUrl) {
                    return ajaxUrl;
                }
                return null;
            },
            dataType: 'json',
            delay: 250,
            processResults: function (data, params) {
                return {
                    results: data
                };
            }
        }
    });
}

function localidadeDropDownLists(localidadeElement) {
    const urlLocalidades = localidadeElement.attr("ajax-url");

    const valorLocalidades = parseInt($("#valorIdLocalidade").val());

    const urlPreloadLocalidades = urlLocalidades + "/";


    if (urlPreloadLocalidades) {
        $.get(
            urlPreloadLocalidades, valorLocalidades ? { CodigoIBGE: valorLocalidades } : {}
        ).then(function (data) {
            handlePreloadData(data, localidadeElement);

            simpleDropDownList(localidadeElement, 'Informe a Localidade', urlLocalidades);
            localidadeElement.trigger('change');
        });
    }
}

function handlePreloadData(data, element) {

    // This assumes that the data comes back as an array of data objects
    for (var d = 0; d < data.length; d++) {
        var item = data[d];
        // Create the DOM option that is pre-selected by default
        var option = new Option(item.text, item.id, true, true);
        // Append it to the select
        element.append(option);
    }
}

function preloadSelectedItem(element) {

    //Carrega o valor do campo caso seja um Edit (atributo value do elemento tem um id nele)
    var valor = parseInt(element.attr("value"));
    if (valor) {
        $.ajax({
            url: element.attr("ajax-url") + "/" + valor.toString()
        }).then(function (data) {

            // Update the selected options that are displayed
            element.trigger('change');
        });
    }
}

function defineSelectedItem(element) {

    //Carregar o valor caso seja um Edit (atributo value do elemento tem um id nele)
    var valor = parseInt(element.attr("value"));
    if (valor !== NaN && valor > 0) {
        $.ajax({
            url: element.attr("data-ajax--url") + "/" + valor.toString()
        }).then(function (data) {
            // This assumes that the data comes back as an array of data objects
            for (var d = 0; d < data.length; d++) {
                var item = data[d];
                // Create the DOM option that is pre-selected by default
                var option = new Option(item.text, item.id, true, true);
                // Append it to the select
                element.append(option);
            }
            // Update the selected options that are displayed
            element.trigger('change');
        });
    }
}