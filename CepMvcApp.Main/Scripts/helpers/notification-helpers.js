﻿$(document).ajaxError(function (e, jqxhr, settings, exception) {

    e.stopPropagation();
    if (jqxhr !== null) {
        obj = JSON.parse(jqxhr.responseText);
        if (obj && (!obj.type || obj.Type !== "Validation")) {
            errorNotification(obj.Message);
        }
    }

});

function monitorNotificationMessages() {

    try {
        var message = $('#returnMessage').val();
        var errorMessage = $('#returnErrorMessage').val();
        $('#returnMessage').val("");
        $('#returnErrorMessage').val("");

        if (message && message.length && message.trim().length > 0) {
            successNotification(message);
        }

        if (errorMessage && errorMessage.length && errorMessage.trim().length > 0) {
            errorNotification(errorMessage);
        }
    }
    finally {
        setTimeout(monitorNotificationMessages, 500);
    }
}

$(document).ready(function () {
    monitorNotificationMessages();

    $(window).on("resize", () => {
        var temGrid = getDefaultGridObject().length === 1;
        temGrid ? getDefaultGridObject().trigger("click") : {};
    });
});

function errorNotification(message) {
    toastr.options = {
        "closeOnHover": false,
        "tapToDismiss": false,
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-full-width",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.error(message);
}

function successNotification(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-full-width",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.success(message);
}

function confirmDelete(callback) {
    swal({
        title: "Confirmar",
        text: "Excluir registro ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: true
    }, function () {
        callback();
    });
}