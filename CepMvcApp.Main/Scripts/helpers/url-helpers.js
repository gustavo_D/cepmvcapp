﻿function concatIdToUrl(url, id) {
    if (url === "" || id === "")
    {
        alert("Favor fornecer a url e o Id");
        return;
    }

    if (url.endsWith('/')) {
        url += id;
    }
    else {
        url += '/' + id;
    }

    return url;
}