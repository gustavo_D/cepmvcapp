﻿using CepMvcApp.Data.Mappings;
using CepMvcApp.Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CepMvcApp.Data
{
    public partial class EntitiesDbContext : DbContext
    {
        public EntitiesDbContext() : base("name=EntitiesDbContext")
        {
            var adapter = (IObjectContextAdapter)this;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = 60;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new BairrosMapping());
            modelBuilder.Configurations.Add(new BairrosXLocalidadesMapping());
            modelBuilder.Configurations.Add(new CodigosEnderecemantoPostalMapping());
            modelBuilder.Configurations.Add(new DiscagemDiretaDistanciaMapping());
            modelBuilder.Configurations.Add(new LocalidadesIBGEMapping());
            modelBuilder.Configurations.Add(new UnidadesFederativasMapping());
        }

        public virtual DbSet<Bairros> Bairros { get; set; }
        public virtual DbSet<BairrosXLocalidades> BairrosXLocalidades { get; set; }
        public virtual DbSet<CodigosEnderecamentoPostal> CodigosEnderecamentoPostal { get; set; }
        public virtual DbSet<DiscagemDiretaDistancia> DiscagemDiretaDistancia { get; set; }
        public virtual DbSet<LocalidadesIBGE> LocalidadesIBGE { get; set; }
        public virtual DbSet<UnidadesFederativas> UnidadesFederativas { get; set; }
    }
}
