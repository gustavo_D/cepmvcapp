﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class CodigosEnderecemantoPostalMapping : EntityTypeConfiguration<CodigosEnderecamentoPostal>
    {
        public CodigosEnderecemantoPostalMapping()
        {
            ToTable("CodigosEnderecamentoPostal");

            //TODO: Configurar corretamente os relacionamentos.
            HasRequired(t => t._Bairros).WithMany().HasForeignKey(x => x.IDBairro);
            HasRequired(t => t._LocalidadesIBGE).WithMany().HasForeignKey(x => x.IDLocalidade);
            HasRequired(t => t._UnidadesFederativas).WithMany().HasForeignKey(x => x.IDUnidadeFederativa);
            HasRequired(t => t._DiscagemDiretaDistancia).WithMany().HasForeignKey(x => x.IDDiscagemDiretaDistancia);
        }
    }
}

