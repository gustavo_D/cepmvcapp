﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class BairrosXLocalidadesMapping : EntityTypeConfiguration<BairrosXLocalidades>
    {
        public BairrosXLocalidadesMapping()
        {
            ToTable("BairrosXLocalidades");

            //TODO: Configurar corretamente os relacionamentos.
            HasRequired(t => t._Bairros).WithMany().HasForeignKey(x => x.IDBairro);
            HasRequired(t => t._LocalidadesIBGE).WithMany().HasForeignKey(x => x.IDLocalidadeIBGE);
        }
    }
}
