﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class UnidadesFederativasMapping : EntityTypeConfiguration<UnidadesFederativas>
    {
        public UnidadesFederativasMapping()
        {
            ToTable("UnidadesFederativas");
        }
    }
}