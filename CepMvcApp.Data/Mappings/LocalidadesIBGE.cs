﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class LocalidadesIBGEMapping : EntityTypeConfiguration<LocalidadesIBGE>
    {
        public LocalidadesIBGEMapping()
        {
            ToTable("LocalidadesIBGE");

            //TODO: Configurar corretamente os relacionamentos.
            HasRequired(t => t._UnidadesFederativas).WithMany().HasForeignKey(x => x.IDUnidadeFederativa);
            HasRequired(t => t.DiscagemDiretaDistancia).WithMany().HasForeignKey(x => x.IDDiscagemDiretaDistancia);
        }
    }
}