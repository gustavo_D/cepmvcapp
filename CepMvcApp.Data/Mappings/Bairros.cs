﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class BairrosMapping : EntityTypeConfiguration<Bairros>
    {
        public BairrosMapping()
        {
            ToTable("Bairros");
        }
    }
}
