﻿using CepMvcApp.Model;
using System.Data.Entity.ModelConfiguration;

namespace CepMvcApp.Data.Mappings
{
    class DiscagemDiretaDistanciaMapping : EntityTypeConfiguration<DiscagemDiretaDistancia>
    {
        public DiscagemDiretaDistanciaMapping()
        {
            ToTable("DiscagemDiretaDistancia");
        }
    }
}