﻿namespace CepMvcApp.Model
{
    public partial class UnidadesFederativas
    {
        public UnidadesFederativas() {;}

        public long ID { get; set; }
        public string CodigoIBGE { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
    }
}
