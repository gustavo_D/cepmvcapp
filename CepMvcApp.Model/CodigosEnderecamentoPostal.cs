﻿namespace CepMvcApp.Model
{
    public partial class CodigosEnderecamentoPostal
    {
        public CodigosEnderecamentoPostal() {;}

        public long ID { get; set; }

        public string Valor { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }

        public long? IDBairro { get; set; }
        public long IDLocalidade { get; set; }
        public long IDUnidadeFederativa { get; set; }
        public long IDDiscagemDiretaDistancia { get; set; }

        public virtual Bairros _Bairros { get; set; }
        public virtual LocalidadesIBGE _LocalidadesIBGE { get; set; }
        public virtual UnidadesFederativas _UnidadesFederativas { get; set; }
        public virtual DiscagemDiretaDistancia _DiscagemDiretaDistancia { get; set; }
    }
}
