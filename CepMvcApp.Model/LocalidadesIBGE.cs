﻿namespace CepMvcApp.Model
{
    public partial class LocalidadesIBGE
    {
        public LocalidadesIBGE() {; }

        public long ID { get; set; }
        public string CodigoIBGE {get; set;}
        public string Gia { get; set; }
        public string Siafi { get; set; }
        public string Nome { get; set; }

        public long IDUnidadeFederativa { get; set; }
        public long IDDiscagemDiretaDistancia { get; set; }

        public virtual UnidadesFederativas _UnidadesFederativas { get; set; }
        public virtual DiscagemDiretaDistancia DiscagemDiretaDistancia { get; set; }
    }
}
