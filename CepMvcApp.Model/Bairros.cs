﻿namespace CepMvcApp.Model
{
    public partial class Bairros
    {
        public Bairros() {;}

        public long ID { get; set; }
        public string Nome { get; set; }
    }
}
