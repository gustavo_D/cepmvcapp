﻿namespace CepMvcApp.Model
{
    public partial class BairrosXLocalidades
    {
        public BairrosXLocalidades() {;}

        public long ID { get; set; }

        public long IDBairro { get; set; }
        public long IDLocalidadeIBGE { get; set; }

        public virtual Bairros _Bairros { get; set; }
        public virtual LocalidadesIBGE _LocalidadesIBGE { get; set; }
    }
}
