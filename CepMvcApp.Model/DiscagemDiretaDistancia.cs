﻿namespace CepMvcApp.Model
{
    public partial class DiscagemDiretaDistancia
    {
        public DiscagemDiretaDistancia() {;}

        public long ID { get; set; }
        public string DDD { get; set; }
    }
}
