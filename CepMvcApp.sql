USE [master]
GO
/****** Object:  Database [CepMvcApp]    Script Date: 4/1/2022 11:43:49 PM ******/
CREATE DATABASE [CepMvcApp]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CepMvcApp', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CepMvcApp.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CepMvcApp_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\CepMvcApp_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CepMvcApp] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CepMvcApp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CepMvcApp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CepMvcApp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CepMvcApp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CepMvcApp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CepMvcApp] SET ARITHABORT OFF 
GO
ALTER DATABASE [CepMvcApp] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CepMvcApp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CepMvcApp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CepMvcApp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CepMvcApp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CepMvcApp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CepMvcApp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CepMvcApp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CepMvcApp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CepMvcApp] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CepMvcApp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CepMvcApp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CepMvcApp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CepMvcApp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CepMvcApp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CepMvcApp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CepMvcApp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CepMvcApp] SET RECOVERY FULL 
GO
ALTER DATABASE [CepMvcApp] SET  MULTI_USER 
GO
ALTER DATABASE [CepMvcApp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CepMvcApp] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CepMvcApp] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CepMvcApp] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CepMvcApp] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CepMvcApp] SET QUERY_STORE = OFF
GO
USE [CepMvcApp]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CepMvcApp]
GO
/****** Object:  Table [dbo].[Bairros]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bairros](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](400) NOT NULL,
 CONSTRAINT [PK_Bairros] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BairrosXLocalidades]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BairrosXLocalidades](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[IDBairro] [bigint] NOT NULL,
	[IDLocalidadeIBGE] [bigint] NOT NULL,
 CONSTRAINT [PK_BairrosXLocalidades] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CodigosEnderecamentoPostal]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CodigosEnderecamentoPostal](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Valor] [nvarchar](50) NOT NULL,
	[Logradouro] [nvarchar](400) NULL,
	[Complemento] [nvarchar](400) NULL,
	[IDBairro] [bigint] NULL,
	[IDLocalidade] [bigint] NOT NULL,
	[IDUnidadeFederativa] [bigint] NOT NULL,
	[IDDiscagemDiretaDistancia] [bigint] NOT NULL,
 CONSTRAINT [PK_CodigosEnderecamentoPostal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DiscagemDiretaDistancia]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiscagemDiretaDistancia](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DDD] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DiscagemDiretaDistancia] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LocalidadesIBGE]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocalidadesIBGE](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoIBGE] [nvarchar](400) NOT NULL,
	[Gia] [nvarchar](400) NULL,
	[Siafi] [nvarchar](400) NULL,
	[Nome] [nvarchar](400) NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[IDUnidadeFederativa] [bigint] NOT NULL,
	[IDDiscagemDiretaDistancia] [bigint] NOT NULL,
 CONSTRAINT [PK_LocalidadesIBGE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UnidadesFederativas]    Script Date: 4/1/2022 11:43:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadesFederativas](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoIBGE] [nvarchar](50) NOT NULL,
	[Nome] [nvarchar](400) NOT NULL,
	[Sigla] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UnidadesFederativas] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Bairros] ON 

GO
INSERT [dbo].[Bairros] ([ID], [Nome]) VALUES (1, N'Serra')
GO
SET IDENTITY_INSERT [dbo].[Bairros] OFF
GO
SET IDENTITY_INSERT [dbo].[BairrosXLocalidades] ON 

GO
INSERT [dbo].[BairrosXLocalidades] ([ID], [IDBairro], [IDLocalidadeIBGE]) VALUES (1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[BairrosXLocalidades] OFF
GO
SET IDENTITY_INSERT [dbo].[CodigosEnderecamentoPostal] ON 

GO
INSERT [dbo].[CodigosEnderecamentoPostal] ([ID], [Valor], [Logradouro], [Complemento], [IDBairro], [IDLocalidade], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (1, N'30240180', N'Rua Pouso Alto', NULL, 1, 1, 1, 1)
GO
INSERT [dbo].[CodigosEnderecamentoPostal] ([ID], [Valor], [Logradouro], [Complemento], [IDBairro], [IDLocalidade], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (10, N'32900000', NULL, NULL, NULL, 10, 1, 1)
GO
INSERT [dbo].[CodigosEnderecamentoPostal] ([ID], [Valor], [Logradouro], [Complemento], [IDBairro], [IDLocalidade], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (11, N'39400092', N'Rua Oleg�rio Silveira', NULL, NULL, 11, 1, 1)
GO
INSERT [dbo].[CodigosEnderecamentoPostal] ([ID], [Valor], [Logradouro], [Complemento], [IDBairro], [IDLocalidade], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (12, N'90160093', N'Avenida Ipiranga', N'de 1075 a 2579 - lado �mpar', NULL, 12, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[CodigosEnderecamentoPostal] OFF
GO
SET IDENTITY_INSERT [dbo].[DiscagemDiretaDistancia] ON 

GO
INSERT [dbo].[DiscagemDiretaDistancia] ([ID], [DDD]) VALUES (1, N'31')
GO
SET IDENTITY_INSERT [dbo].[DiscagemDiretaDistancia] OFF
GO
SET IDENTITY_INSERT [dbo].[LocalidadesIBGE] ON 

GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (1, N'3106200', NULL, N'4123', N'Belo Horizonte', CAST(N'2022-03-31T18:56:17.220' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (2, N'3118601', NULL, N'4371', N'Contagem', CAST(N'2022-04-01T17:17:49.033' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (3, N'3106705', NULL, N'4133', N'Betim', CAST(N'2022-04-01T17:59:20.193' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (4, N'3144805', NULL, N'4895', N'Nova Lima', CAST(N'2022-04-01T18:11:33.037' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (10, N'3130101', NULL, N'4601', N'Igarap�', CAST(N'2022-04-01T22:30:18.313' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (11, N'3143302', NULL, N'4865', N'Montes Claros', CAST(N'2022-04-01T22:31:55.287' AS DateTime), 1, 1)
GO
INSERT [dbo].[LocalidadesIBGE] ([ID], [CodigoIBGE], [Gia], [Siafi], [Nome], [DataCadastro], [IDUnidadeFederativa], [IDDiscagemDiretaDistancia]) VALUES (12, N'4314902', NULL, N'8801', N'Porto Alegre', CAST(N'2022-04-01T22:33:20.057' AS DateTime), 1, 1)
GO
SET IDENTITY_INSERT [dbo].[LocalidadesIBGE] OFF
GO
SET IDENTITY_INSERT [dbo].[UnidadesFederativas] ON 

GO
INSERT [dbo].[UnidadesFederativas] ([ID], [CodigoIBGE], [Nome], [Sigla]) VALUES (1, N'31', N'Minas Gerais', N'MG')
GO
INSERT [dbo].[UnidadesFederativas] ([ID], [CodigoIBGE], [Nome], [Sigla]) VALUES (2, N'35', N'S�o Paulo', N'SP')
GO
SET IDENTITY_INSERT [dbo].[UnidadesFederativas] OFF
GO
/****** Object:  Index [IX_BairrosXLocalidades]    Script Date: 4/1/2022 11:43:49 PM ******/
ALTER TABLE [dbo].[BairrosXLocalidades] ADD  CONSTRAINT [IX_BairrosXLocalidades] UNIQUE NONCLUSTERED 
(
	[IDBairro] ASC,
	[IDLocalidadeIBGE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BairrosXLocalidades_1]    Script Date: 4/1/2022 11:43:49 PM ******/
ALTER TABLE [dbo].[BairrosXLocalidades] ADD  CONSTRAINT [IX_BairrosXLocalidades_1] UNIQUE NONCLUSTERED 
(
	[IDLocalidadeIBGE] ASC,
	[IDBairro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CodigosEnderecamentoPostal]    Script Date: 4/1/2022 11:43:49 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_CodigosEnderecamentoPostal] ON [dbo].[CodigosEnderecamentoPostal]
(
	[Valor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UnidadesFederativas]    Script Date: 4/1/2022 11:43:49 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_UnidadesFederativas] ON [dbo].[UnidadesFederativas]
(
	[CodigoIBGE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UnidadesFederativas_1]    Script Date: 4/1/2022 11:43:49 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_UnidadesFederativas_1] ON [dbo].[UnidadesFederativas]
(
	[Sigla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LocalidadesIBGE] ADD  CONSTRAINT [DF_LocalidadesIBGE_DataCadastro]  DEFAULT (getdate()) FOR [DataCadastro]
GO
ALTER TABLE [dbo].[BairrosXLocalidades]  WITH CHECK ADD  CONSTRAINT [FK_BairrosXLocalidades_Bairros] FOREIGN KEY([IDBairro])
REFERENCES [dbo].[Bairros] ([ID])
GO
ALTER TABLE [dbo].[BairrosXLocalidades] CHECK CONSTRAINT [FK_BairrosXLocalidades_Bairros]
GO
ALTER TABLE [dbo].[BairrosXLocalidades]  WITH CHECK ADD  CONSTRAINT [FK_BairrosXLocalidades_LocalidadesIBGE] FOREIGN KEY([IDLocalidadeIBGE])
REFERENCES [dbo].[LocalidadesIBGE] ([ID])
GO
ALTER TABLE [dbo].[BairrosXLocalidades] CHECK CONSTRAINT [FK_BairrosXLocalidades_LocalidadesIBGE]
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal]  WITH CHECK ADD  CONSTRAINT [FK_CodigosEnderecamentoPostal_Bairros] FOREIGN KEY([IDBairro])
REFERENCES [dbo].[Bairros] ([ID])
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal] CHECK CONSTRAINT [FK_CodigosEnderecamentoPostal_Bairros]
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal]  WITH CHECK ADD  CONSTRAINT [FK_CodigosEnderecamentoPostal_DiscagemDiretaDistancia] FOREIGN KEY([IDDiscagemDiretaDistancia])
REFERENCES [dbo].[DiscagemDiretaDistancia] ([ID])
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal] CHECK CONSTRAINT [FK_CodigosEnderecamentoPostal_DiscagemDiretaDistancia]
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal]  WITH CHECK ADD  CONSTRAINT [FK_CodigosEnderecamentoPostal_LocalidadesIBGE] FOREIGN KEY([IDLocalidade])
REFERENCES [dbo].[LocalidadesIBGE] ([ID])
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal] CHECK CONSTRAINT [FK_CodigosEnderecamentoPostal_LocalidadesIBGE]
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal]  WITH CHECK ADD  CONSTRAINT [FK_CodigosEnderecamentoPostal_UnidadesFederativas] FOREIGN KEY([IDUnidadeFederativa])
REFERENCES [dbo].[UnidadesFederativas] ([ID])
GO
ALTER TABLE [dbo].[CodigosEnderecamentoPostal] CHECK CONSTRAINT [FK_CodigosEnderecamentoPostal_UnidadesFederativas]
GO
ALTER TABLE [dbo].[LocalidadesIBGE]  WITH CHECK ADD  CONSTRAINT [FK_LocalidadesIBGE_DiscagemDiretaDistancia] FOREIGN KEY([IDDiscagemDiretaDistancia])
REFERENCES [dbo].[DiscagemDiretaDistancia] ([ID])
GO
ALTER TABLE [dbo].[LocalidadesIBGE] CHECK CONSTRAINT [FK_LocalidadesIBGE_DiscagemDiretaDistancia]
GO
ALTER TABLE [dbo].[LocalidadesIBGE]  WITH CHECK ADD  CONSTRAINT [FK_LocalidadesIBGE_UnidadesFederativas] FOREIGN KEY([IDUnidadeFederativa])
REFERENCES [dbo].[UnidadesFederativas] ([ID])
GO
ALTER TABLE [dbo].[LocalidadesIBGE] CHECK CONSTRAINT [FK_LocalidadesIBGE_UnidadesFederativas]
GO
USE [master]
GO
ALTER DATABASE [CepMvcApp] SET  READ_WRITE 
GO
